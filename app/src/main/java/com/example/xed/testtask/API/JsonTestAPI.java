package com.example.xed.testtask.API;

import java.util.HashMap;


import retrofit2.Call;

import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by гыук on 05.10.2017.
 */

public interface JsonTestAPI   {

    @POST("http://ip.jsontest.com/")
    Call<GetIpModel> getIp();

    @POST("http://date.jsontest.com")
    Call<GetDataTimeModel> getDataTime();

    @GET("http://echo.jsontest.com/key/{value}")
    Call<EchoResponce> getEcho(@Path("value")String echo);


    @POST("http://validate.jsontest.com/")
    Call<HashMap<String,String>>getValidation(@Query("json") String body);
}

