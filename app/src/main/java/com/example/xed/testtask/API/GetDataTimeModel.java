package com.example.xed.testtask.API;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by гыук on 05.10.2017.
 */

public class GetDataTimeModel {

    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("milliseconds_since_epoch")
    @Expose
    private Long millisecondsSinceEpoch;
    @SerializedName("date")
    @Expose
    private String date;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Long getMillisecondsSinceEpoch() {
        return millisecondsSinceEpoch;
    }

    public void setMillisecondsSinceEpoch(Long millisecondsSinceEpoch) {
        this.millisecondsSinceEpoch = millisecondsSinceEpoch;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
