package com.example.xed.testtask.Fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.xed.testtask.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by гыук on 05.10.2017.
 */

public class AboutMeFragment extends Fragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View rootView =
                inflater.inflate(R.layout.about_me_fragment_layout, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;

    }

    @OnClick(R.id.button2)
    public void onClickButon() {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"avatarofdark@yandex.ru"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Приглашаем вас на работу");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {

        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
