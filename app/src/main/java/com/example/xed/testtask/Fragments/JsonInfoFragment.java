package com.example.xed.testtask.Fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.example.xed.testtask.API.EchoResponce;
import com.example.xed.testtask.API.GetDataTimeModel;
import com.example.xed.testtask.API.GetIpModel;
import com.example.xed.testtask.App;
import com.example.xed.testtask.MyListView;
import com.example.xed.testtask.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by гыук on 05.10.2017.
 */

public class JsonInfoFragment extends Fragment {

    @BindView(R.id.ip_wiew)
    TextView ipView;
    @BindView(R.id.time)
    TextView timeView;
    @BindView(R.id.milliseconds_since_epoch)
    TextView msView;
    @BindView(R.id.date)
    TextView dateView;
    @BindView(R.id.address)
    TextView addressView;
    @BindView(R.id.echo_edit_text)
    EditText echoEditText;
    @BindView(R.id.echo_text_responce)
    TextView echoTextView;
    @BindView(R.id.validation_edit_text)
    EditText validationEditText;
    @BindView(R.id.validation_list_view)
    MyListView validarionListView;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        View rootView =
                inflater.inflate(R.layout.json_info_fragment_layout, container, false);
        ButterKnife.bind(this, rootView);

        addressView.setText("Address: " + App.getBaseURL());
        validationEditText.setText("{\"key\":\"value\"}");
        App.getApi().getIp().enqueue(new Callback<GetIpModel>() {
            @Override
            public void onResponse(Call<GetIpModel> call, Response<GetIpModel> response) {
                ipView.setText("ip: " + response.body().getIp());
            }

            @Override
            public void onFailure(Call<GetIpModel> call, Throwable t) {

            }
        });


        App.getApi().getDataTime().enqueue(new Callback<GetDataTimeModel>() {
            @Override
            public void onResponse(Call<GetDataTimeModel> call, Response<GetDataTimeModel> response) {
                timeView.setText("time: " + response.body().getTime());
                msView.setText("milliseconds_since_epoch: " + response.body().getMillisecondsSinceEpoch());
                dateView.setText("date: " + response.body().getDate());
            }

            @Override
            public void onFailure(Call<GetDataTimeModel> call, Throwable t) {
            }
        });

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        return rootView;


    }

    @OnClick(R.id.submit_button)
    public void onSubmitClick() {

        App.getApi().getEcho(echoEditText.getText().toString()).enqueue(new Callback<EchoResponce>() {
            @Override
            public void onResponse(Call<EchoResponce> call, Response<EchoResponce> response) {
                echoTextView.setVisibility(View.VISIBLE);
                echoTextView.setText("ECHO answered:" + response.body().getValueFromKey());
            }

            @Override
            public void onFailure(Call<EchoResponce> call, Throwable t) {

            }
        });

    }


    @OnClick(R.id.submit_button_validation)
    public void onSubmitValidationClick() {
        App.getApi().getValidation(validationEditText.getText().toString()).enqueue(new Callback<HashMap<String, String>>() {
            @Override
            public void onResponse(Call<HashMap<String, String>> call, Response<HashMap<String, String>> response) {
                List listValuses = new ArrayList(response.body().values());
                List listKeys = new ArrayList(response.body().keySet());
                String[] responceInArray = new String[response.body().size()];
                for (int i = 0; i < response.body().size(); i++) {
                    responceInArray[i] = new String(listKeys.get(i) + ": " + listValuses.get(i));
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(),
                        android.R.layout.simple_list_item_1, responceInArray);

                validarionListView.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<HashMap<String, String>> call, Throwable t) {

            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }
}
