package com.example.xed.testtask;

        import android.app.Application;

        import com.example.xed.testtask.API.JsonTestAPI;


        import retrofit2.Retrofit;
        import retrofit2.converter.gson.GsonConverterFactory;


public class App extends Application {

    private static JsonTestAPI jsonTestAPI;
    private static Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("http://www.jsontest.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonTestAPI = retrofit.create(JsonTestAPI.class);
    }

    public static JsonTestAPI getApi() {
        return jsonTestAPI;
    }

    public static String getBaseURL(){
        return retrofit.baseUrl().toString();
    }


}
