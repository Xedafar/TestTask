package com.example.xed.testtask;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.xed.testtask.Fragments.AboutMeFragment;
import com.example.xed.testtask.Fragments.JsonInfoFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    FragmentTransaction fTrans;
    AboutMeFragment aboutMeFragment;
    JsonInfoFragment jsonInfoFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        aboutMeFragment = new AboutMeFragment();
        jsonInfoFragment = new JsonInfoFragment();

        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.fragment_space, jsonInfoFragment);
        fTrans.commit();

    }

    @OnClick(R.id.two_button)
    public void clickFromAboutME(){
        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.fragment_space, aboutMeFragment);
        fTrans.commit();
    }

    @OnClick(R.id.one_button)
    public void clickFromJsonInfo(){
        fTrans = getFragmentManager().beginTransaction();
        fTrans.replace(R.id.fragment_space, jsonInfoFragment);
        fTrans.commit();
    }
}
