package com.example.xed.testtask.API;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by гыук on 05.10.2017.
 */

public class EchoResponce {

    @SerializedName("key")
    @Expose
    private String key;

    public String getValueFromKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
